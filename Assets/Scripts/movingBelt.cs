﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingBelt : MonoBehaviour
{
    private Rigidbody rBody;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        rBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 position = rBody.position;
        rBody.position += transform.right * speed * Time.fixedDeltaTime;
        rBody.MovePosition(position);
    }
}
